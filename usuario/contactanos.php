<?php 
session_start();
$sesion = $_SESSION['username'];
if(!isset($sesion)){
    header("location: index.php");

}?>
<!DOCTYPE html>
<html>
<?php include("head.php");?>
<body>
<?php include("header.php");?>
</div>
    <div class="contact-clean">
        <form method="post">
            <h2 class="text-center">Contact us</h2>
            <div class="form-group"><input class="form-control" type="text" name="name" placeholder="Name"></div>
            <div class="form-group"><input class="form-control " type="email" name="email" placeholder="Email">
                <!--<small class="form-text text-danger">Please enter a correct email address.</small>!--><!--is-invalid!--></div>
            <div class="form-group"><textarea class="form-control" rows="14" name="message" placeholder="Message"></textarea></div>
            <div class="form-group"><button class="btn btn-primary" type="submit" style="background-color:rgb(107,197,235);">send </button></div>
        </form>
</div>
    </div>
    <?php include("footer.php");?>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>