<!DOCTYPE html>
<html>

<?php include("head.php");?>
<body>
    <div>
        <nav class="navbar navbar-light navbar-expand-md navigation-clean-button">
            <div class="container"><a class="navbar-brand" href="bienvenido.php">SISTEMA DE VENTAS</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse"
                    id="navcol-1">
                    <ul class="nav navbar-nav mr-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="bienvenido.php">Inicio</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="carrito.php">Carrito</a></li>
                    </ul><span class="navbar-text actions"> <a class="login"><?php echo $_SESSION['username'] ?></a><a class="btn btn-light action-button" role="button" href="conexion/salir.php">Salir</a></span></div>
            </div>
        </nav>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>