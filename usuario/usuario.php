<!DOCTYPE html>
<html>

<?php include("head.php");?>
<body>
<div>
<nav class="navbar navbar-light navbar-expand-md navigation-clean-button">
            <div class="container"><a class="navbar-brand" href="index.php">SISTEMA DE VENTAS</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse"
                    id="navcol-1">
                    <ul class="nav navbar-nav mr-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="index.php">Inicio</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="usuario.php">Registrarse</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="admin/login.php">administrador</a></li>
                    </ul><span class="navbar-text actions"><a class="btn btn-light action-button" role="button" href="login.php">Ingresar</a></span>
            </div>
        </nav>
    </div>
    <div class="register-photo">
        <div class="form-container">
            <div class="image-holder">
                
            <form method="post" action="conexion/reg-usuario.php">
                <h2 class="text-center"><strong>Registro </strong>de Usuario.</h2>
                <div class="form-group"><input class="form-control" type="text" name="name" placeholder="Nombre" required></div>
                <div class="form-group"><input class="form-control" type="text" name="userr" placeholder="Usuario" required></div>
                <div class="form-group"><input class="form-control" type="email" name="email" placeholder="Email" required></div>
                <div class="form-group"><input class="form-control" type="number" name="phone" placeholder="Numero de telefono" required></div>
                <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Password" required></div>
                <div class="form-group"><button class="btn btn-primary btn-block" type="submit" style="background-color:rgb(107,197,235);">Registrarse</button></div>
            </form>
        </div>
    </div>
    </div>
    <div class="footer-dark">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a>
                        <p class="copyright">Sistema de Ventas © 2019</p>
                    </div>
                </div>
            </div>
        </footer>
</div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>