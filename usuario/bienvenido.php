<?php
session_start();
$sesion = $_SESSION['username'];
if(!isset($sesion)){
    header("location: index.php");

}?>
<!DOCTYPE html>
<html>

<?php include("head.php");?>
<body>
<?php include("header.php");?>
    <div class="highlight-phone">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="intro">
                        <h2>¿QUIENES SOMOS?</h2>
                        <p>Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. Aliqua sed justo ligula.</p><a class="btn btn-primary" role="button" href="contactanos.php" style="background-color:rgb(5,218,180);">CONTACTANOS</a></div>
                </div>
                <div class="col-sm-4">
                    <div class="d-none d-md-block iphone-mockup"><img src="../assets/img/iphone.svg" class="device">
                        <div class="screen"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="projects-clean">
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Productos.</h2>
            </div>
            <div class="row projects">
                <div class="col-sm-6 col-lg-4 item"><img class="img-fluid" src="assets/img/desk.jpg">
                    <h3 class="name">Project Name</h3>
                    <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p>
                    <a class="btn btn-primary" role="button" href="#" style="background-color:rgb(5,218,180);padding:7px 24px;margin-top:28px;">Add to Cart.</a></div>
                <div
                    class="col-sm-6 col-lg-4 item"><img class="img-fluid" src="assets/img/minibus.jpeg">
                    <h3 class="name">Project Name</h3>
                    <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p>
                    <a class="btn btn-primary" role="button" href="#" style="background-color:rgb(5,218,180);padding:7px 24px;margin-top:28px;">Add to Cart.</a></div>
            <div
                class="col-sm-6 col-lg-4 item"><img class="img-fluid" src="assets/img/desk.jpg">
                <h3 class="name">Project Name</h3>
                <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.</p>
                <a class="btn btn-primary" role="button" href="#" style="background-color:rgb(5,218,180);padding:7px 24px;margin-top:28px;">Add to Cart.</a></div>
    </div>
    </div>
    </div>-->
    <div class="projects-horizontal">
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Productos.</h2>
            </div>
            <div class="row projects">
                <?php
        include("../admin/conexion_admin/conexion.php"); 
        $sql="SELECT * FROM producto";
        $result=mysqli_query($conectar,$sql);

        while($mostrar=mysqli_fetch_array($result)){
        ?>   
          <div class="col-sm-6 item">
                    <div class="row">
                        <div class="col-md-12 col-lg-5"><a><?php echo '<img src="../admin/'.$mostrar['img_pro'].'"'?></a></div>
                        <div class="col">
                            <h3 class="name"><?php echo $mostrar['name_pro'] ?></h3>
                            <h6><?php echo $mostrar['price_pro'] ?> Bs.</h6>
                            <p class="description"><?php echo $mostrar['description_pro'] ?></p><a class="btn btn-primary" role="button" href="#" style="background-color:rgb(5,218,180);padding:7px 24px;margin-top:28px;">Agegar a carrito</a></div>
                    </div>
          </div>
        <?php 
          }
          ?>
                
            </div>
        </div>
    </div>
    <?php include("footer.php");?>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>