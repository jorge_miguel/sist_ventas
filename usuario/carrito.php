<?php
session_start();
$sesion = $_SESSION['username'];
if(!isset($sesion)){
    header("location: index.php");

}?>
<!DOCTYPE html>
<html>

<?php include("head.php");?>
<body>
<?php include("header.php");?>
<div class="product-big-title-area wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Carrito De Compras</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End titulo de la pagina -->
    
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">              
                <div class="col-md-12">
                    <div class="product-content-right">
                        <div class="woocommerce">
                                <div class="table-responsive">
                                    <table cellspacing="0" class="shop_table cart">
                                        <thead>
                                            <tr>
                                                <th class="product-thumbnail"><i class="fa fa-file-image-o" aria-hidden="true"></i> Foto</th>
                                                <th class="product-name"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Producto</th>
                                                <th class="product-price"><i class="fa fa-usd" aria-hidden="true"></i> Precio</th>
                                                <th class="product-quantity">Cantidad</th>
                                                <th class="product-subtotal"><i class="fa fa-usd" aria-hidden="true"></i> Total</th>
                                                <th><i class="fa fa-cart-plus" aria-hidden="true"></i> Cantidad</th>
                                                <th><i class="fa fa-times" aria-hidden="true"></i> Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="cart_item wow fadeIn">
                                                <td class="product-thumbnail">
                                                  <img width="145" height="145" alt="" class="shop_thumbnail" src="img/.jpg">
                                                </td>

                                                <td class="product-name">
                                                  
                                                </td>

                                                <td class="product-price">
                                                    <span class="amount">Bs.
                                                    </span> 
                                                </td>

                                                <td class="product-quantity">
                                                    <div class="quantity buttons_added">
                                                        
                                                    </div>
                                                </td>

                                                <td class="product-subtotal">
                                                    <span class="amount">Bs.</span> 
                                                </td>
                                                <td>
                                                    <input type="number" name="cantidad" id="cantidad" value="3" style="width:60px"required>
                                                </td>
                                                <td>
                                                    <a onClick="return confirm('¿Está seguro que desea eliminar esta compra?')" class="btn btn-danger"><span class="fa fa-times" aria-hidden="true"></span></a>
                                                </td>
                                            </tr> 
                                        </tbody>
                                    </table>
                                </div>
                            <div class="cart-collaterals wow fadeIn">
                            
                                <div class="cart_totals col-sm-6 col-xs-12">
                                    <h2>Total Carrito</h2>

                                    <table cellspacing="0">
                                        <tbody>
                                            <tr class="shipping">
                                                <th>Costo De Envío</th>
                                                <td>Bs.
                                               </td>
                                            </tr>
                                            <tr>
                                                <td>Subtotal</td>
                                                <td>Bs.</td>
                                            </tr>

                                            <tr class="order-total">
                                                <th>Total Pedido</th>
                                                <td><strong><span class="amount">Bs.</span></strong> </td>
                                            </tr>
                                            <tr>
                                                <td>Confirme Su Compra</td>
                                                <td>
                                                    <form id="form1" method="post" action="confirmacion.php">
                                                        <input type="submit" name="button" id="button" value="Comprar" class="btn btn-success">
                                                    </form>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>                        
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    <?php include("footer.php");?>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>