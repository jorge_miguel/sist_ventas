<!DOCTYPE html>
<html>
<?php include("head.php");?>
<body>
    <h1 style="text-align: center; padding: 20px;">Ingreso al sistema</h1>
    <div class="login-clean" style="padding-top:50px;">
        <form method="post" action="conexion/login.php">
            <div class="form-group"><input class="form-control" type="text" name="userr" placeholder="Ingrese su usuario"></div>
            <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Password"></div>
            <div class="form-group"><button class="btn btn-primary btn-block" type="submit">ingresar al sistema</a></button></div>
            <div style="text-align: center;"><p>¿No se registro aun?</p>
            <p><a href="usuario.php">Pinche aqui :)</a></p></div>
        </form>
    </div>
<div class="footer-dark">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a>
                        <p class="copyright">Sistema de Ventas © 2019</p>
                    </div>
                </div>
            </div>
        </footer>
</div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>