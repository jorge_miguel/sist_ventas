<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema de Ventas</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="assets/css/Highlight-Phone.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/Projects-Clean.css">
    <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Lightbox-Gallery.css">
    <link rel="stylesheet" href="assets/css/estilos.css">
</head>

<body>
<div>
        <nav class="navbar navbar-light navbar-expand-md navigation-clean-button">
            <div class="container"><a class="navbar-brand" href="index.php">SISTEMA DE VENTAS</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse"
                    id="navcol-1">
                    <ul class="nav navbar-nav mr-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link" href="index.php">Inicio</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="usuario/usuario.php">Registrarse</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="admin/login.php">administrador</a></li>
                    </ul><span class="navbar-text actions"><a class="btn btn-light action-button" role="button" href="usuario/login.php">Ingresar</a></span>
                </div>
            </div>
        </nav>
</div>
<div class="highlight-phone">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="intro">
                        <h2>¿QUIENES SOMOS?</h2>
                        <p>Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. Aliqua sed justo ligula.</p><a class="btn btn-primary" role="button" href="#" style="background-color:rgb(5,218,180);">CONTACTANOS</a></div>
                </div>
                <div class="col-sm-4">
                    <div class="d-none d-md-block iphone-mockup"><img src="assets/img/iphone.svg" class="device">
                        <div class="screen"></div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="photo-gallery">
    <div class="container">
        <div class="intro">
            <h2 class="text-center">Nuestros productos</h2>
            <p class="text-center">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae.</p>
        </div>
        <div class="row photos">
            <div class="col-sm-6 col-md-4 col-lg-3 item"><img class="img-fluid" src="assets/img/desk.jpg" /></a></div>
            <div class="col-sm-6 col-md-4 col-lg-3 item"><img class="img-fluid" src="assets/img/building.jpg" /></div>
            <div class="col-sm-6 col-md-4 col-lg-3 item"><img class="img-fluid" src="assets/img/loft.jpg" /></div>
            <div class="col-sm-6 col-md-4 col-lg-3 item"><img class="img-fluid" src="assets/img/building.jpg" /></div>
            <div class="col-sm-6 col-md-4 col-lg-3 item"><img class="img-fluid" src="assets/img/loft.jpg" /></div>
            <div class="col-sm-6 col-md-4 col-lg-3 item"><img class="img-fluid" src="assets/img/desk.jpg" /></div>
        </div>
    </div>
</div>
<div class="footer-dark">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a>
                        <p class="copyright">Sistema de Ventas © 2019</p>
                    </div>
                </div>
            </div>
        </footer>
</div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>