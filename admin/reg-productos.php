<?php
session_start();
$sesion = $_SESSION['username'];
if(!isset($sesion)){
    header("location: login.php");

}?>
<!DOCTYPE html>
<html>

<?php include("head-admin.php");?>
<body>
<?php include("header-admin.php");?>
    <div class="register-photo">
        <div class="form-container">
            <div class="image-holder"></div>
            <form method="post" action="reg-producto.php" enctype="multipart/form-data">
                <h2 class="text-center"><strong>Registro </strong>de Productos.</h2>
                <div class="form-group">
                    <input class="form-control" type="text" name="name_pro" placeholder="Nombre">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" name="code_pro" placeholder="Codigo del producto">
                </div>
                <div class="form-group">
                    <input class="form-control" type="number" name="stock_pro" placeholder="Cantidad a registrar">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" name="description_pro" placeholder="descripcion del producto">
                </div>
                <div class="form-group">
                    <input class="form-control" type="number" name="price_pro" placeholder="Precio del producto">
                </div>
                <div class="form-group">
                    <div class="panel">Subir Imagen</div>
                    <input type="file" name="img_pro" id="img_pro" required>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-block" type="submit" style="background-color:rgb(107,197,235);">Registrar</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>