<?php
session_start();
$sesion = $_SESSION['username'];
if(!isset($sesion)){
    header("location: login.php");

}?>
<!DOCTYPE html>
<html>

<?php include("head-admin.php");?>
<body>
<?php include("header-admin.php");?>

<h1 style="text-align: center;padding: 15px;">Listado de Usuarios</h1>
<br>
<div class="container">
<div class="content-wrapper">
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
      </div>
      <div class="box-body">
       <table class="table table-bordered table-striped dt-responsive tablas">
        <thead>
         <tr>
           <th style="width:10px">#</th>
           <th>Nombre</th>
           <th>Usuario</th>
           <th>Correo</th>
           <th>Telefono</th>
           <th>Contraseña</th>
           <th>Fecha de Registro</th>
           <th>Acciones</th>
         </tr> 
        </thead>
        <tbody>   
          <?php
        include("conexion_admin/conexion.php"); 
        $sql="SELECT * FROM usuario";
        $result=mysqli_query($conectar,$sql);

        while($mostrar=mysqli_fetch_array($result)){
        ?>   
          <tr>
            <td><?php echo $mostrar['id'] ?></td>
            <td><?php echo $mostrar['name'] ?></td>
            <td><?php echo $mostrar['userr'] ?></td>
            <td><?php echo $mostrar['email'] ?></td>
            <td><?php echo $mostrar['phone'] ?></td>
            <td><?php echo $mostrar['password'] ?></td>
            <td><?php echo $mostrar['datee'] ?></td>
            <td>
              <div class="btn-group"> 
                <button class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                <button class="btn btn-danger"><a href="conexion_admin/delete-usu.php?id=<?php echo $mostrar['id']; ?>""><i class="fa fa-times"></i></a></button>
              </div>  
            </td>
          </tr>
        <?php 
          }
          ?>
        </tbody>
       </table>
      </div>
    </div>
  </section>
</div>
</div>

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>