<?php
session_start();
$sesion = $_SESSION['username'];
if(!isset($sesion)){
    header("location: login.php");

}?>
<!DOCTYPE html>
<html>

<?php include("head-admin.php");?>
<body>
<?php include("header-admin.php");?>

<h1 style="text-align: center;padding: 15px;">Listado de Productos</h1>
<br>
<div class="container">
<div class="content-wrapper">
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
      </div>
      <div class="box-body">
       <table class="table table-bordered table-striped dt-responsive tablas">
        <thead>
         <tr>
           <th style="width:10px">#</th>
           <th>Nombre</th>
           <th>Imagen</th>
           <th>Codigo</th>
           <th>Cantidad</th>
           <th>Descripcion</th>
           <th>Precio Bs.</th>
           <th>Fecha de registro</th>
           <th>Acciones</th>
         </tr> 
        </thead>
        <tbody>
        <?php
        include("conexion_admin/conexion.php"); 
        $sql="SELECT * FROM producto";
        $result=mysqli_query($conectar,$sql);

        while($mostrar=mysqli_fetch_array($result)){
        ?>   
          <tr>
            <td><?php echo $mostrar['id_pro'] ?></td>
            <td><?php echo $mostrar['name_pro'] ?></td>
            <td style="text-align: center;"><?php echo '<img src="'.$mostrar['img_pro'].'" width="140" heigth="140">'?></td>
            <td><?php echo $mostrar['code_pro'] ?></td>
            <td><?php echo $mostrar['stock_pro'] ?></td>
            <td><?php echo $mostrar['description_pro'] ?></td>
            <td><?php echo $mostrar['price_pro'] ?></td>
            <td><?php echo $mostrar['datee_pro'] ?></td>
            <td>
              <div class="btn-group"> 
                <button class="btn btn-warning"><a href=""><i class="fa fa-pencil"></i></a></button>
                <button class="btn btn-danger"><a href="conexion_admin/delete-pro.php?id_pro=<?php echo $mostrar['id_pro']; ?>"><i class="fa fa-times"></a></i></button>
              </div>  
            </td>
          </tr>
        <?php 
          }
          ?>
        </tbody>
       </table>
      </div>
    </div>
  </section>
</div>
</div>

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>