
<!DOCTYPE html>
<html>

<?php include("head-admin.php");?>
<body>
    <div>
        <nav class="navbar navbar-light navbar-expand-md navigation-clean-button">
            <div class="container"><a class="navbar-brand" href="bienvenido.php">SISTEMA DE VENTAS</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse"
                    id="navcol-1">
                    <ul class="nav navbar-nav mr-auto">
                        <li class="dropdown"><a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Registros</a>
                            <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item" role="presentation" href="reg-productos.php">Productos</a>
                            </div>
                        </li>
                        <li class="dropdown"><a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Listados de registro</a>
                            <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item" role="presentation" href="lista-productos.php">Listas de productos</a>
                                <a class="dropdown-item" role="presentation" href="lista-usuarios.php">Lista de usuarios</a>
                                <a class="dropdown-item" role="presentation" href="lista-pedidos.php">Lista de pedidos</a>
                            </div>
                        </li>
                    </ul><span class="navbar-text actions"> <a class="login"><?php echo $_SESSION['username'] ?></a><a class="btn btn-light action-button" role="button" href="conexion_admin/salir.php">Salir</a></span></div>
            </div>
        </nav>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>