<!DOCTYPE html>
<html>
<?php include("head-admin.php");?>
<body>
    <h1 style="text-align: center; padding: 35px;">Sistema de ventas Administracion</h1>
    <div class="login-clean" style="padding-top:100px;">
        <form method="post" action="conexion_admin/login-adm.php">
            <div class="form-group"><input class="form-control" type="text" name="user_adm" placeholder="Usuario"></div>
            <div class="form-group"><input class="form-control" type="password" name="password_adm" placeholder="Password"></div>
            <div class="form-group"><button class="btn btn-primary btn-block" type="submit">ingresar al sistema</a></button></div>
        </form>
    </div>
    <div class="footer-dark">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a>
                        <p class="copyright">Sistema de Ventas © 2019</p>
                    </div>
                </div>
            </div>
        </footer>
</div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>