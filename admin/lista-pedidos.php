<?php
session_start();
$sesion = $_SESSION['username'];
if(!isset($sesion)){
    header("location: login.php");

}?>
<!DOCTYPE html>
<html>

<?php include("head-admin.php");?>
<body>
<?php include("header-admin.php");?>

<h1 style="text-align: center;padding: 15px;">Listado de Pedidos</h1>
<br>
<div class="content-wrapper">
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
      </div>
      <div class="box-body">
       <table class="table table-bordered table-striped dt-responsive tablas">
        <thead>
         <tr>
           <th style="width:10px">#</th>
           <th>Nombre</th>
           <th>imagen</th>
           <th>Codigo</th>
           <th>Cantidad</th>
           <th>Descripcion</th>
           <th>Precio Bs.</th>
           <th>Fecha de peticion</th>
           <th>Acciones</th>
         </tr> 
        </thead>
        <tbody>   
          <tr>
            <td>1</td>
            <td>almohada</td>
            <td style="text-align: center;"><img src="../assets/img/minibus.jpeg" class="img-thumbnail" width="100px"></td>
            <td>12345</td>
            <td>10</td>
            <td>suavecita</td>
            <td>50</td>
            <td>2017-12-11 12:05:32</td>
            <td>
              <div class="btn-group"> 
                <button class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                <button class="btn btn-danger"><i class="fa fa-times"></i></button>
              </div>  
            </td>
          </tr>
          <tr>
            <td>1</td>
            <td>almohada</td>
            <td style="text-align: center;"><img src="../assets/img/minibus.jpeg" class="img-thumbnail" width="100px"></td>
            <td>12345</td>
            <td>10</td>
            <td>suavecita</td>
            <td>50</td>
            <td>2017-12-11 12:05:32</td>
            <td>
              <div class="btn-group"> 
                <button class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                <button class="btn btn-danger"><i class="fa fa-times"></i></button>
              </div>  
            </td>
          </tr>
        </tbody>
       </table>
      </div>
    </div>
  </section>
</div>


    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>